import SignUp from './components/signUp';
import { AuthProvider } from './context/authContext';
import {BrowserRouter as Router,Switch,Route } from "react-router-dom"

import Product from "./components/product";
import Add from "./components/add";
import Edit from "./components/edit"

function App() {
  return (
    <Router>
      <AuthProvider>
        <Switch>
          <Route exact path="/" component={SignUp}></Route>
          <Route exact path="/add" component={Add}></Route>
          <Route exact path="/edit/:id" component={Edit}></Route>
          <Route exact path="/dashBoard" component={Product}></Route>
        </Switch>
      </AuthProvider>
    </Router>
  );
}
export default App;
