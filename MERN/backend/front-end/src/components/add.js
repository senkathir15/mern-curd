import React from "react";
import { useRef, useContext } from "react";
import { Form, Button, Card, Alert } from "react-bootstrap";
import { AuthContext } from "../context/authContext";
import axios from "axios";
import { Link, useHistory } from "react-router-dom";

export default function Add() {
  //  const [error, setError] = React.useState("");
  const [image, setImage] = React.useState("");
  const { addProduct, seterror } = useContext(AuthContext);
  const ProductNameRef = useRef();
  const priceRef = useRef();
  const descriptionRef = useRef();
  const imageRef = useRef();
  const value = {};
  const history = useHistory();

  const uploadImage = (e) => {
    setImage(e.target.files[0]);
    console.log(e.target.files[0]);
  };
  const handelSubmit = (e) => {
    e.preventDefault();

    const formData = new FormData();
    formData.append("name", ProductNameRef.current.value);
    formData.append("price", priceRef.current.value);
    formData.append("description", descriptionRef.current.value);
    formData.append("image", image);

    addProduct(formData);
  };

  return (
    <>
      <Card className="card m-auto">
        <Card.Body>
          <h2 className="text-center mb-4">Add product Details</h2>
          {/* {error && <Alert>{error}</Alert>} */}
          <Form onSubmit={handelSubmit} encType="multipart/form-data">
            <Form.Group className="mb-3" name="fristName">
              <Form.Label>
                ProductName<span className="requried">*</span>
              </Form.Label>
              <Form.Control
                name="name"
                type="text"
                placeholder="Enter productName"
                ref={ProductNameRef}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="passWord">
              <Form.Label>
                price<span className="requried">*</span>
              </Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter price"
                required
                ref={priceRef}
                name="price"
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="passWord">
              <Form.Label>
                Description<span className="requried">*</span>
              </Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Title"
                ref={descriptionRef}
                required
                name="description"
              />
            </Form.Group>

            <Form.Group className="mb-3" id="Email">
              <Form.Label>
                image<span className="requried">*</span>
              </Form.Label>
              <Form.Control
                name="image"
                type="file"
                placeholder="please upload jpeg,png format"
                onChange={uploadImage}
                required
                ref={imageRef}
              />
              <Form.Text className="text-muted">
                please upload file in jpeg,png format
              </Form.Text>
            </Form.Group>
            <div className="btns">
              <Button type="submit" variant="primary">
                save
              </Button>
            </div>
          </Form>
          <div className="w-100 text-center mt-2"></div>
        </Card.Body>
      </Card>
    </>
  );
}
