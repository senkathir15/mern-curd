import React from "react";
import styled from "styled-components";
import { AuthContext } from "../context/authContext";
import { Link, useHistory } from "react-router-dom";

export default function ProductItem() {
  const { item, deleteProduct, fetchProduct } = React.useContext(AuthContext);

  return (
    <Section>
      {item.length > 0 || <h1>No product</h1>}
      {item.map((item) => {
        return (
          <div className="card">
            <img src={`image/${item.image}`}></img>
            <div>
              <p>{item.name}</p>
              <h5>${item.price}</h5>
              <div>Description:</div>
              <p>{item.description}</p>
              <Link to={`/edit/${item._id}`} key={item._id}>
                {" "}
                <button>edit </button>
              </Link>
              <button
                onClick={() => {
                  deleteProduct(item._id);
                }}
              >
                Delete
              </button>
            </div>
          </div>
        );
      })}
    </Section>
  );
}
const Section = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  max-width: 1000px;
  margin: 0 auto;

  .card {
    dispaly: flex;
    max-width: 15rem;
    margin: 2rem auto;
    background-color: #f3e9e9;
    color: rgb(252, 71, 71);
    border-radius: 14px;
    padding: 1rem;
    box-shadow: 0 1px 18px 10px rgba(0, 0, 0, 0.25);

    img {
      width: 200px;
      height: 200px;
      display: block;
      object-fit: cover;
    }
    p {
      text-align: center;
      color: black;
      margin: 0;
    }
    h3 {
      margin: 0;
    }
    button {
      width: 100%;
      color: white;
      border: 1px solid green;
      background: blue;
      border-radius: 10px;
      padding: 10px;
      font-size: 1rem;
      font-weight: bold;
      margin-top: 10px;
    }
    h1 {
      text-align: center;
      color: red;
      margin: 0 auto;
    }
  }
`;
