import React from "react";
import styled from "styled-components";
import ProductItem from "./productItem";
import { Link, useHistory } from "react-router-dom";
import { Form, Button, Card, Alert } from "react-bootstrap";
import { AuthContext } from "../context/authContext";
import { useContext, useEffect } from "react";

export default function Product() {
  const { error, fetchProduct } = useContext(AuthContext);

  useEffect(() => {
    fetchProduct();
  }, []);

  return (
    <Section>
      <h1>Our Products</h1>
      {error && <Alert>{error}</Alert>}
      <Btns>
        <button>
          <Link className="btn" to="/add">
            Add Product
          </Link>
        </button>
      </Btns>

      <div className="products">
        <ProductItem />
      </div>
    </Section>
  );
}

const Section = styled.section`
  width: 100%;

  h1 {
    text-align: center;
  }
  .products {
    padding: 0 100px;
    @media (max-width: 450px) {
      padding: 0 50px;
    }
    .btns {
      display: flex;
      justify-content: center;
      align-items: center;
    }
    .btn {
      width: 100%;
      color: white;
      border: 1px solid green;
      background: blue;
      border-radius: 10px;
      padding: 10px;
      font-size: 1rem;
      font-weight: bold;
    }
  }
`;

const Btns = styled.div`
  text-align: center;
`;
