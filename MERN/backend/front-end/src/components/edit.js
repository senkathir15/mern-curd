import React from "react";
import { useRef, useContext, useEffect } from "react";
import { Form, Button, Card, Alert } from "react-bootstrap";
import { AuthContext } from "../context/authContext";
import { useParams, Link } from "react-router-dom";

export default function Edit() {
  const [error, setError] = React.useState("");
  //   const [image, setImage] = React.useState("");

  const param = useParams();
  console.log(param.id);
  const { singleitem, fetchsingleProduct, editProduct } =
    useContext(AuthContext);
  const ProductNameRef = useRef();
  const priceRef = useRef();
  const descriptionRef = useRef();
  //  const imageRef = useRef();
  const value = {};

  useEffect(() => {
    fetchsingleProduct(param.id);
  }, []);

  const handelSubmit = (e) => {
    e.preventDefault();

    value[ProductNameRef.current.name] = ProductNameRef.current.value;
    value[priceRef.current.name] = priceRef.current.value;
    value[descriptionRef.current.name] = descriptionRef.current.value;

    editProduct(value, param.id);
  };

  return (
    <>
      <Card className="card m-auto">
        <Card.Body>
          <h2 className="text-center mb-4">Edit product Details</h2>
          {error && <Alert>{error}</Alert>}
          <Form onSubmit={handelSubmit}>
            <Form.Group className="mb-3" name="fristName">
              <Form.Label>
                ProductName<span className="requried">*</span>
              </Form.Label>
              <Form.Control
                name="name"
                type="text"
                placeholder="Enter productName"
                ref={ProductNameRef}
                defaultValue={singleitem.name}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="passWord">
              <Form.Label>
                price<span className="requried">*</span>
              </Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter price"
                required
                ref={priceRef}
                defaultValue={singleitem.price}
                name="price"
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="passWord">
              <Form.Label>
                Description<span className="requried">*</span>
              </Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Title"
                ref={descriptionRef}
                required
                name="description"
                defaultValue={singleitem.description}
              />
            </Form.Group>

            <div className="btns">
              <Button type="submit" variant="primary">
                update
              </Button>
            </div>
          </Form>
          <div className="w-100 text-center mt-2"></div>
        </Card.Body>
      </Card>
    </>
  );
}
